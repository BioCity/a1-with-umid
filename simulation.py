"""CSC148 Assignment 1 - Simulation

=== CSC148 Fall 2018 ===
Department of Computer Science,
University of Toronto

=== Module description ===
This contains the main Simulation class that is actually responsible for
creating and running the simulation. You'll also find the function `sample_run`
here at the bottom of the file, which you can use as a starting point to run
your simulation on a small configuration.

Note that we have provided a fairly comprehensive list of attributes for
Simulation already. Y
ou may add your own *private* attributes, but should not
remove any of the existing attributes.
"""
# You may import more things from these modules (e.g., additional types from
# typing), but you may not import from any other modules.
from typing import Dict, List, Any

import algorithms
from algorithms import Direction
from entities import Person, Elevator
from visualizer import Visualizer


class Simulation:
    """The main simulation class.

    === Attributes ===
    arrival_generator: the algorithm used to generate new arrivals.
    elevators: a list of the elevators in the simulation
    moving_algorithm: the algorithm used to decide how to move elevators
    num_floors: the number of floors
    visualizer: the Pygame visualizer used to visualize this simulation
    waiting: a dictionary of people waiting for an elevator
             (keys are floor numbers, values are the list of waiting people)
    disembarked: time used to disembark for each person
    """
    arrival_generator: algorithms.ArrivalGenerator
    elevators: List[Elevator]
    moving_algorithm: algorithms.MovingAlgorithm
    num_floors: int
    visualizer: Visualizer
    waiting: Dict[int, List[Person]]
    disembarked: List[int]

    def __init__(self,
                 config: Dict[str, Any]) -> None:
        """Initialize a new simulation using the given configuration."""

        # Initialize the visualizer.
        # Note that this should be called *after* the other attributes
        # have been initialized.
        self.num_floors = config['num_floors']
        self.elevators = list()
        self.waiting = dict()
        self.disembarked = []
        for _ in range(config['num_elevators']):
            self.elevators.append(Elevator(config['elevator_capacity']))

        self.moving_algorithm = config['moving_algorithm']
        self.arrival_generator = config['arrival_generator']

        self.visualizer = Visualizer(self.elevators,
                                     self.num_floors,
                                     config['visualize'])
    ############################################################################
    # Handle rounds of simulation.
    ############################################################################

    def run(self, num_rounds: int) -> Dict[str, Any]:
        """Run the simulation for the given number of rounds.

        Return a set of statistics for this simulation run, as specified in the
        assignment handout.

        Precondition: num_rounds >= 1.

        Note: each run of the simulation starts from the same initial state
        (no people, all elevators are empty and start at floor 1).
        """
        for i in range(num_rounds):
            self.visualizer.render_header(i)

            # Stage 1: generate new arrivals
            self._generate_arrivals(i)

            # Stage 2: leave elevators
            self._handle_leaving()

            # Stage 3: board elevators
            self._handle_boarding()

            # Stage 4: move the elevators using the moving algorithm
            self._move_elevators()

            # Pause for 1 second
            self.visualizer.wait(1)

        return self._calculate_stats()

    def _generate_arrivals(self, round_num: int) -> None:
        """Generate and visualize new arrivals."""

        temp = self.arrival_generator.generate(round_num)

        for key, item in temp.items():
            if key in self.waiting:
                self.waiting[key].extend(item)
            else:
                self.waiting[key] = item

        self.visualizer.show_arrivals(self.waiting)

    def _handle_leaving(self) -> None:
        """Handle people leaving elevators."""
        for elev in self.elevators:
            marked = []
            for passenger in elev.passengers:
                if elev.floor == passenger.target:
                    marked.append(passenger)
            while len(marked) != 0:
                passenger = marked.pop()
                self.disembarked.append(passenger.wait_time)
                elev.passengers.remove(passenger)
                self.visualizer.show_disembarking(passenger, elev)

    def _handle_boarding(self) -> None:
        """Handle boarding of people and visualize."""
        for key, people in self.waiting.items():
            for elev in self.elevators:
                if key == elev.floor:
                    while len(people) != 0 and elev.fullness() < 1:
                        temp = people.pop(0)
                        elev.add_passengers(temp)
                        self.visualizer.show_boarding(temp, elev)

    def _move_elevators(self) -> None:
        """Move the elevators in this simulation.

        Use this simulation's moving algorithm to move the elevators.
        """
        move = self.moving_algorithm.move_elevators(self.elevators,
                                                    self.waiting,
                                                    self.num_floors)
        self.visualizer.show_elevator_moves(self.elevators, move)
        self._handle_boarding()
    ############################################################################
    # Statistics calculations
    ############################################################################

    def _calculate_stats(self) -> Dict[str, int]:
        """
        Report the statistics for the current run of this simulation.
        """
        total_people = 0
        max_time = -1
        min_time = -1
        avg_time = -1
        for i in self.elevators:
            total_people += int(i.fullness() * i.capacity)
        for key in self.waiting.items():
            total_people += len(key[1])
        if len(self.disembarked) != 0:
            max_time = max(self.disembarked)
            min_time = min(self.disembarked)
            avg_time = round(sum(self.disembarked) / len(self.disembarked))

        return {
            'num_iterations': 15,
            'total_people': len(self.disembarked) + total_people,
            'people_completed': len(self.disembarked),
            'max_time': max_time,
            'min_time': min_time,
            'avg_time': avg_time
        }


def sample_run() -> Dict[str, int]:
    """Run a sample simulation, and return the simulation statistics."""
    config = {
        'num_floors': 6,
        'num_elevators': 6,
        'elevator_capacity': 3,
        'num_people_per_round': 2,
        # Random arrival generator with 6 max floors and 2 arrivals per round.
        'arrival_generator': algorithms.FileArrivals(6, 'sample_arrivals.csv'),
        'moving_algorithm': algorithms.PushyPassenger(),
        'visualize': True
    }

    sim = Simulation(config)
    stats = sim.run(15)
    return stats


if __name__ == '__main__':
    # Uncomment this line to run our sample simulation (and print the
    # statistics generated by the simulation).
    print(sample_run())

    import python_ta
    python_ta.check_all(config={
        'extra-imports': ['entities', 'visualizer', 'algorithms', 'time'],
        'max-nested-blocks': 4
    })
