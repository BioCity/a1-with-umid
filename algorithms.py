"""CSC148 Assignment 1 - Algorithms

=== CSC148 Fall 2018 ===
Department of Computer Science,
University of Toronto

=== Module Description ===

This file contains two sets of algorithms: ones for generating new arrivals to
the simulation, and ones for making decisions about how elevators should move.

As with other files, you may not change any of the public behaviour (attributes,
methods) given in the starter code, but you can definitely add new attributes
and methods to complete your work here.

See the 'Arrival generation algorithms' and 'Elevator moving algorithsm'
sections of the assignment handout for a complete description of each algorithm
you are expected to implement in this file.
"""
import csv
from enum import Enum
import random
from typing import Dict, List, Optional

from entities import Person, Elevator


###############################################################################
# Arrival generation algorithms
###############################################################################
class ArrivalGenerator:
    """An algorithm for specifying arrivals at each round of the simulation.

    === Attributes ===
    max_floor: The maximum floor number for the building.
               Generated people should not have a starting or target floor
               beyond this floor.
    num_people: The number of people to generate, or None if this is left
                up to the algorithm itself.

    === Representation Invariants ===
    max_floor >= 2
    num_people is None or num_people >= 0
    """
    max_floor: int
    num_people: Optional[int]

    def __init__(self, max_floor: int, num_people: Optional[int]) -> None:
        """Initialize a new ArrivalGenerator.

        Preconditions:
            max_floor >= 2
            num_people is None or num_people >= 0
        """
        self.max_floor = max_floor
        self.num_people = num_people

    def generate(self, round_num: int) -> Dict[int, List[Person]]:
        """Return the new arrivals for the simulation at the given round.

        The returned dictionary maps floor number to the people who
        arrived starting at that floor.

        You can choose whether to include floors where no people arrived.
        """
        raise NotImplementedError


class RandomArrivals(ArrivalGenerator):
    """Generate a fixed number of random people each round.

    Generate 0 people if self.num_people is None.

    For our testing purposes, this class *must* have the same initializer
    as ArrivalGenerator. So if you choose to to override the initializer,
    sure to keep the header the same!

    Hint: look up the 'sample' function from random.
    """

    def __init__(self, max_floor: int, num_people: Optional[int]) -> None:
        """Initializer of Random Arrivals"""

        ArrivalGenerator.__init__(self, max_floor, num_people)

    def generate(self, round_num: int) -> Dict[int, List[Person]]:
        """Generate a fixed number of random people each round.

        Generate 0 people if self.num_people is None.

        For our testing purposes, this class *must* have the same initializer
        as ArrivalGenerator. So if you choose to to override the initializer,
        sure to keep the header the same!

        Hint: look up the 'sample' function from random.
        """
        people = dict()

        for _ in range(self.num_people):
            floors = list(range(1, self.max_floor + 1))

            start = random.choice(floors)
            floors.remove(start)
            target = random.choice(floors)
            temp = Person(start, target)

            if start not in people.keys():
                people[start] = [temp]
            else:
                people[start].append(temp)

        return people


class FileArrivals(ArrivalGenerator):
    """Generate arrivals from a CSV file.
    """
    _waiting: Dict[int, List[Person]]

    def __init__(self, max_floor: int, filename: str) -> None:
        """Initialize a new FileArrivals algorithm from the given file.

        The num_people attribute of every FileArrivals instance is set to None,
        since the number of arrivals depends on the given file.

        Precondition:
            <filename> refers to a valid CSV file, following the specified
            format and restrictions from the assignment handout.
        """

        ArrivalGenerator.__init__(self, max_floor, None)

        # We've provided some of the "reading from csv files" boilerplate code
        # for you to help you get started.
        self._waiting = dict()

        with open(filename) as csvfile:
            reader = csv.reader(csvfile)
            for line in reader:
                per = list(map(int, line))
                self._waiting[per[0]] = dict()
                for i in range(1, len(per), 2):
                    if per[i] in self._waiting[per[0]]:
                        self._waiting[per[0]][per[i]].append(Person(per[i],
                                                                    per[i + 1]))
                    else:
                        self._waiting[per[0]][per[i]] = [Person(per[i],
                                                                per[i + 1])]

    def generate(self, round_num: int) -> Dict[int, List[Person]]:
        """Generate a fixed number of random people each round.

        Generate 0 people if self.num_people is None.

        For our testing purposes, this class *must* have the same initializer
        as ArrivalGenerator. So if you choose to to override the initializer,
        sure to keep the header the same!
        """

        if round_num in self._waiting.keys():
            return self._waiting[round_num]
        else:
            return dict()


###############################################################################
# Elevator moving algorithms
###############################################################################


class Direction(Enum):
    """
    The following defines the possible directions an elevator can move.
    This is output by the simulation's algorithms.

    The possible values you'll use in your Python code are:
        Direction.UP, Direction.DOWN, Direction.STAY
    """
    UP = 1
    STAY = 0
    DOWN = -1


class MovingAlgorithm:
    """An algorithm to make decisions for moving an elevator at each round.
    """

    def move_elevators(self,
                       elevators: List[Elevator],
                       waiting: Dict[int, List[Person]],
                       max_floor: int) -> List[Direction]:
        """Return a list of directions for each elevator to move to.

        As input, this method receives the list of elevators in the simulation,
        a dictionary mapping floor number to a list of people waiting on
        that floor, and the maximum floor number in the simulation.

        Note that each returned direction should be valid:
            - An elevator at Floor 1 cannot move down.
            - An elevator at the top floor cannot move up.
        """
        raise NotImplementedError


class RandomAlgorithm(MovingAlgorithm):
    """A moving algorithm that picks a random direction for each elevator.
    """

    def move_elevators(self,
                       elevators: List[Elevator],
                       waiting: Dict[int, List[Person]],
                       max_floor: int) -> List[Direction]:
        """Return a list of directions for each elevator to move to.

        As input, this method receives the list of elevators in the simulation,
        a dictionary mapping floor number to a list of people waiting on
        that floor, and the maximum floor number in the simulation.
         """

        move = []
        for i in elevators:
            choice = 0
            if i.floor == 1:
                choice = random.choice([Direction.UP, Direction.STAY])
                move.append(choice)
            elif i.floor == max_floor:
                choice = random.choice([Direction.DOWN, Direction.STAY])
                move.append(choice)

            else:
                choice = random.choice([Direction.UP, Direction.STAY,
                                        Direction.DOWN])
                move.append(choice)
                i.floor += Direction(choice).value

        return move


class PushyPassenger(MovingAlgorithm):
    """A moving algorithm that preferences the first passenger on each elevator.

    If the elevator is empty, it moves towards the *lowest* floor that has at
    least one person waiting, or stays still if there are no people waiting.

    If the elevator isn't empty, it moves towards the target floor of the
    *first* passenger who boarded the elevator.
    """

    def _best_match(self, elevator: Elevator, array: List[int]) -> int:
        """Return the best match for the person located on the lowest floor"""
        min_diff = array[0]
        for i in range(1, len(array)):
            if abs(min_diff - elevator.floor) > abs(elevator.floor - array[i]):
                min_diff = array[i]

        return min_diff

    def move_elevators(self,
                       elevators: List[Elevator],
                       waiting: Dict[int, List[Person]],
                       max_floor: int) -> List[Direction]:
        """Return a list of directions for each elevator to move to.

        As input, this method receives the list of elevators in the simulation,
        a dictionary mapping floor number to a list of people waiting on
        that floor, and the maximum floor number in the simulation.
         """
        move = list()
        for i in elevators:

            if i.fullness() == 0:
                array = [m[0] for m in waiting.items() if
                         len(m[1]) != 0 and i.floor != m[0]]

                if len(array) != 0:
                    target = self._best_match(i, array)
                else:
                    target = i.floor
            else:
                target = i.passengers[0].target

            if target < i.floor:
                move.append(Direction.DOWN)
            elif target > i.floor:
                move.append(Direction.UP)
            else:
                move.append(Direction.STAY)

            i.floor += move[-1].value

        return move


class ShortSighted(MovingAlgorithm):
    """A moving algorithm that preferences the closest possible choice.

    If the elevator is empty, it moves towards the *closest* floor that has at
    least one person waiting, or stays still if there are no people waiting.

    If the elevator isn't empty, it moves towards the closest target floor of
    all passengers who are on the elevator.

    In this case, the order in which people boarded does *not* matter.
    """

    def _best_match(self, elevator: Elevator, array: List[int]) -> int:
        """Return the best match for the person located on the lowest floor"""
        min_diff = array[0]
        for i in range(1, len(array)):
            if abs(min_diff - elevator.floor) > abs(elevator.floor - array[i]):
                min_diff = array[i]

        return min_diff

    def _best_match_people(self, elevator: Elevator) -> int:
        """Return the best match for the person located on the lowest floor"""
        array = [i for i in elevator.passengers if i.target != elevator.floor]
        min_diff = array[0]
        for i in range(1, len(array)):
            if abs(min_diff.target - elevator.floor) > \
                    abs(array[i].target - elevator.floor):
                min_diff = array[i]

        return min_diff.target

    def move_elevators(self,
                       elevators: List[Elevator],
                       waiting: Dict[int, List[Person]],
                       max_floor: int) -> List[Direction]:
        """Return a list of directions for each elevator to move to.

        As input, this method receives the list of elevators in the simulation,
        a dictionary mapping floor number to a list of people waiting on
        that floor, and the maximum floor number in the simulation.
         """
        move = list()
        for i in elevators:

            if i.fullness() == 0:
                array = [m[0] for m in waiting.items() if
                         len(m[1]) != 0 and i.floor != m[0]]
                if len(array) != 0:
                    target = self._best_match(i, array)

                    if target < i.floor:
                        move.append(Direction.DOWN)
                    else:
                        move.append(Direction.UP)
                else:
                    move.append(Direction.STAY)
            else:
                target = self._best_match_people(i)

                if target < i.floor:
                    move.append(Direction.DOWN)
                else:
                    move.append(Direction.UP)

            i.floor += move[-1].value

        return move


if __name__ == '__main__':
    # Don't forget to check your work regularly with python_ta!
    import python_ta

    python_ta.check_all(config={
        'allowed-io': ['__init__'],
        'extra-imports': ['entities', 'random', 'csv', 'enum'],
        'max-nested-blocks': 4,
        'disable': ['R0201']
    })
